import json
import glob
import copy


def extract_evitals_data():
    with open("evitals_data_without_duplicates.json") as file:
        evitals_data = json.load(file)
    return evitals_data


def extract_one_mg_data():
    files = glob.glob("./1mg/1mg_*/*/*.json")
    one_mg_data = []
    for file in files:
        with open(file) as file:
            one_mg_data += json.load(file)
    return one_mg_data


evitals_data = extract_evitals_data()
one_mg_data = extract_one_mg_data()


def update_evitals_data(evitals_data, one_mg_data):
    updated_evitals_data = []

    try:
        evitals_medicine_names = [item.get("name") for item in evitals_data]
        common_and_valid_medicines = [item.get("name") for item in one_mg_data if (item.get("name") in evitals_medicine_names and item.get("is_discontinued") is False)]
        semi_updated_evitals_data = [item for item in evitals_data if item.get("name") in common_and_valid_medicines]
        
        for item1 in one_mg_data:
            for item2 in semi_updated_evitals_data:
                updated_item = copy.deepcopy(item2)
                updated_item["updatedMrp"] = item1.get("price")
                updated_item["type"] = item1.get("type")
                updated_evitals_data.append(updated_item)
    except AttributeError:
        pass

    return updated_evitals_data


updated_evitals_data = update_evitals_data(evitals_data, one_mg_data)

with open("updated_evitals_data.json", "w") as file:
    file.write(json.dumps(updated_evitals_data))




